<?php

/**
 * @file
 * Definition of OdataPluginQueryOdata.
 */

/**
 * OdataPluginQueryOdata
 */
class OdataPluginQueryOdata extends views_plugin_query_default {

  /**
   * Builds the necessary info to execute the query.
   */
  function build(&$view) {
    $view->init_pager();

    // Let the pager modify the query to add limits.
    $this->pager->query();

    $view->build_info['query'] = $this->query();
    $view->build_info['count_query'] = 'count(' . $view->build_info['query'] . ')';
    $view->build_info['query_args'] = array();
  }

  /**
   * Generates a query and a countquery.
   *
   * @param bool $get_count
   *   (Optional) Provide a countquery if this is true, otherwise provide a
   *   normal query. Defaults to FALSE.
   */
  function query($get_count = FALSE) {

    $query = array();

    // Add fields the user has selected in the Views UI.
    if (isset($this->select_fields)) {
      foreach ($this->select_fields as $key => $field) {
        $fields[] = $field;
      }
      // Create a comma separated string.
      $query[] = '$select=' . implode(',', $fields);
    }

    // Add filters.
    if ($this->where) {
      foreach ($this->where as $group_id => $group_filters) {
        foreach ($group_filters['conditions'] as $id => $filter) {
          $filter['operator'] = ($filter['operator'] == 'formula') ? '' : $filter['operator'];
          $filters[] = $filter['field'] . $filter['operator'] . $filter['value'];
        }
      }
      $query[] = '$filter=' . implode("+" . strtolower($this->group_operator) . "+", $filters);
    }

    // Add Sorting Criteria if any.
    if ($this->orderby) {
      foreach ($this->orderby as $id => $field_to_order) {
        $orderby[] = $field_to_order['field'] . '+' . strtolower($field_to_order['direction']);
      }
      // Populate $orderby.
      $query[] = '$orderby=' . implode(',', $orderby);
    }

    // Create the pager.
    if (isset($this->pager)) {
      // Populate $top.
      if ($this->pager->options['items_per_page'] != 0) {
        $query[] = '$top=' . $this->pager->options['items_per_page'];
      }

      // Populate $skip.
      if ($this->pager->options['offset'] != 0) {
        $query[] = '$skip=' . $this->pager->options['offset'];
      }
    }

    return implode('&', $query);
  }

  /**
   * Executes the query and fills the associated view object.
   *
   * Values to set: $view->result, $view->total_rows, $view->execute_time,
   * $view->pager['current_page'].
   *
   * $view->result should contain an array of objects. The array must use a
   * numeric index starting at 0.
   *
   * @param view $view
   *   The view which is executed.
   */
  function execute(&$view) {
    $query = $view->build_info['query'];

    $views_data = odata_views_data();
    $start = microtime(TRUE);

    $reply = odata_request($views_data[$view->base_table]['table']['endpoint'] . '/?' . $query);

    if (!empty($reply['error'])) {
      $view->result = array();
      vsm(t('Error @error:@details', array('@error' => $reply['error'], '@details' => $reply['details'])));
      return;
    }

    $views_data[$view->base_table]['table']['endpoint'];
    foreach ($reply as $key => $values) {
      // If (isset($values['__metadata']))unset($values['__metadata']);
      $rows[$key] = $values;
    }

    if (empty($rows)) {
      $view->result = array();
      vsm(t('No result returned. Please check your query and the endpoint status.'));
      return;
    }

    foreach ($rows as $index => $row) {
      $views_row = array();
      foreach ($row as $name => $value) {
        if (isset($value)) {
          $views_row[$name] = $value;
        }
      }
      $views_result[] = (object) $views_row;
    }
    // Execute count query for pager.
    $count = drupal_http_request($views_data[$view->base_table]['table']['endpoint'] . '/$count?' . $view->build_info['count_query']);

    /*
    if ($this->pager->use_count_query() || !empty($view->get_total_rows)) {
      $this->pager->total_items = $count->data;

      if (!empty($this->pager->options['offset'])) {
        $this->pager->total_items -= $this->pager->options['offset'];
      }
      $this->pager->update_page_info();
    }

    if ($this->pager->use_pager()) {
      $view->total_rows = $this->pager->get_total_items();
    }
    else {
      $view->total_rows = $count->data;
    }
    */

    $view->result = $views_result;

    $view->execute_time = microtime(TRUE) - $start;
  }

  /**
   * Set a LIMIT on the query, specifying a maximum number of results.
   */
  function set_limit($limit) {
    $this->limit = ($limit) ? $limit : 10000000;
  }
}
