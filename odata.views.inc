<?php

/**
 * @file
 * The main file that describes the "overrides" of the original views
 */

/**
 * Implements hook_views_data().
 */
function odata_views_data() {

  $data = array();
  $query = db_query('SELECT title, url, options FROM {odata_endpoints}');

  foreach ($query as $row) {
    $data[$row->title]['table']['group'] = t('Open Data');

    $data[$row->title]['table']['base'] = array(
      'field' => 'vsid',
      'title' => $row->title,
      'help' => t('Directly query test_endpoint to get some live views data.'),
      'query class' => 'odata_query',
    );

    $data[$row->title]['table']['endpoint'] = $row->url . '/' . unserialize($row->options);
  }

  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function odata_views_data_alter(&$datas) {

  require 'odata.admin.inc';
  $query = db_query('SELECT title,options,url FROM {odata_endpoints}');
  foreach ($query as $row) {
    $uri = $row->url . '/$metadata';
    $reply = drupal_http_request($uri);
    $xmltree = odata_xml2array2($reply->data, $row->url);
    $tree = odata_get_entity_properties($xmltree);
    $cat = unserialize($row->options);

    $complexes = odata_get_complex_properties($xmltree);

    if (!empty($tree[$cat])) {
      foreach ($tree[$cat]['Values'] as $index => $value) {
        if (odata_get_type($value['Type']) != '') {
          $datas[$row->title][$value['Name']] = array(
            'title' => $value['Name'],
            'group' => $row->title,
            'field' => array(
              'handler' => _odata_get_field_handler($value['Type']),
              'bundle' => odata_get_type($value['Type']),
              'click sortable' => TRUE,
              'help' => $value['Name'],
            ),
            'sort' => array(
              'help' => t('Order the results of the query as @name.', array('@name' => $value['Name'])),
              'handler' => _odata_get_sort_handler($value['Type']),
            ),
            'filter' => array(
              'handler' => _odata_get_filter_handler($value['Type']),
              'bundle' => odata_get_type($value['Type']),
              'help' => $value['Name'],
            ),
            'argument' => array(
              'handler' => _odata_get_argument_handler($value['Type']),
              'bundle' => odata_get_type($value['Type']),
              'help' => $value['Name'],
            ),
          );
        }
        elseif ($value['Type'] != 'Null') {
          _odata_views_add_complex($datas, $complexes, $row->title, $value['Type'], $value['Name']);
        }
      }
    }
  }
}

/**
 * Adds the Complex Types to the View.
 */
function _odata_views_add_complex(&$datas, &$complexes, $table_name, $value_type, $name = '') {
  $complex_types = odata_get_complex_entity($complexes, $value_type);

  foreach ($complex_types as $i => $v) {

    $datas[$table_name][$name . '-' . $i] = array(
      'title' => $name . ': ' . $i,
      'group' => $table_name,
      'field' => array(
        'handler' => _odata_get_complex_field_handler($v['Type']),
        'bundle' => odata_get_type($v['Type']),
        'click sortable' => TRUE,
        'help' => $name . ':' . $i,
      ),
      'sort' => array(
        'help' => t('Order the results of the query as ' . $name . ':' . $i . '.'),
        'handler' => _odata_get_sort_handler($v['Type']),
      ),
      'filter' => array(
        'handler' => _odata_get_filter_handler($v['Type']),
        'bundle' => odata_get_type($v['Type']),
        'help' => $v['Name'],
      ),
      'argument' => array(
        'handler' => _odata_get_argument_handler($v['Type']),
        'bundle' => odata_get_type($v['Type']),
        'help' => $v['Name'],
      ),
    );
  }
}

/**
 * Implements hook_views_plugins().
 */
function odata_views_plugins() {
  return array(
    'query' => array(
      'odata_query' => array(
        'title' => t('oData Query'), 'help' => t('Query will be generated and run using current oData endpoint.'),
        'handler' => 'OdataPluginQueryOdata',
      ),
    ),
  );
}

/**
 * Returns the correct filter handler for each type.
 */
function _odata_get_filter_handler($value_type) {
  $type = odata_get_type($value_type);
  $ret = 'OdataHandlerFilter';
  if ($type == 'numeric') {
    $ret = 'OdataHandlerFilterNumeric';
  }
  elseif ($type == 'date') {
    $ret = 'OdataHandlerFilterDate';
  }
  elseif ($type == 'boolean') {
    $ret = 'OdataHandlerFilterBooleanOperator';
  }
  elseif ($type == 'markup') {
    $ret = 'OdataHandlerFilterString';
  }
  return $ret;
}

/**
 * Returns the correct field handler for each type.
 */
function _odata_get_field_handler($value_type) {
  $type = odata_get_type($value_type);
  $ret = 'OdataHandlerField';
  if ($type == ' numeric') {
    $ret = 'OdataHandlerFieldNumeric';
  }
  elseif ($type == 'date') {
    $ret = 'OdataHandlerFieldDate';
  }
  elseif ($type == 'boolean') {
    $ret = 'OdataHandlerFieldBoolean';
  }
  elseif ($type == 'markup') {
    $ret = 'OdataHandlerField';
  }
  return $ret;
}

/**
 * Returns an oData sort handler for each data type.
 *
 * @param string $type
 *   Required. Data type to find associated sort handler.
 *
 * @return string
 *   A string containing the appropriate sort handler.
 */
function _odata_get_sort_handler($type) {

  $type = odata_get_type($type);

  if ($type == 'date') {
    return 'OdataHandlerSortDate';
  }

  return 'OdataHandlerSort';
}

/**
 * Returns the correct field handler for each type.
 */
function _odata_get_complex_field_handler($value_type) {
  $type = odata_get_type($value_type);
  $ret = 'OdataHandlerFieldComplex';
  if ($type == 'numeric') {
    $ret = 'OdataHandlerFieldComplex_numeric';
  }
  elseif ($type == 'date') {
    $ret = 'OdataHandlerFieldComplex_date';
  }
  elseif ($type == 'boolean') {
    $ret = 'OdataHandlerFieldComplex_boolean';
  }
  elseif ($type == 'markup') {
    $ret = 'OdataHandlerFieldComplex';
  }
  return $ret;
}

/**
 * Returns the correct argument handler for each type (Contextual Filters).
 */
function _odata_get_argument_handler($value_type) {
  $type = odata_get_type($value_type);
  $ret = 'OdataHandelrArgumentStrings';
  if ($type == 'numeric') {
    $ret = 'OdataHandelrArgumentNumeric';
  }
  elseif ($type == 'date') {
    $ret = 'OdataHandelrArgumentDate';
  }
  elseif ($type == 'boolean') {
    $ret = 'OdataHandelrArgumentBoolean';
  }
  elseif ($type == 'markup') {
    $ret = 'OdataHandelrArgumentString';
  }
  return $ret;
}
