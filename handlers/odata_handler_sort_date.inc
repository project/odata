<?php

/**
 * @file
 * Definition of odata_handler_sort_date.
 */

/**
 * Basic sort handler for dates.
 *
 * Extends odata_handler_sort
 */
class OdataHandlerSortDate extends odata_handler_sort {}
