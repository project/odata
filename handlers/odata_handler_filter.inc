<?php

/**
 * @file
 * Definition of odata_handler_filter.
 */

/**
 * @todo refactor
 */
class OdataHandlerFilter extends views_handler_filter {

  // Overrides of query().
  function query() {
    $this->query->add_where($this->options['group'], $this->real_field, $this->value['value'], $this->operator);
  }
}
