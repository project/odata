<?php
/**
 * @file
 * Adds fields as numbers
 */

/**
 * @todo refactor
 */
class OdataHandlerFieldNumeric extends views_handler_field_numeric {

  function query() {
    $this->field_alias = $this->real_field;
    $this->query->select_fields[] = $this->real_field;
  }
}
