<?php

/**
 * @file
 * Definition of odata_handler_sort.
 */

class OdataHandlerSort extends views_handler_sort {
  /**
   * Called to add the sort to a query.
   */
  function query() {
    $this->query->add_orderby(NULL, NULL, $this->options['order'], $this->real_field);
  }
}
