<?php
/**
 * @file
 * This file contains the information of date arguments
 */

/**
 * @todo refactor
 */
require_once 'odata_handler_filter.inc';

class OdataHandlerArgumentDate extends views_handler_argument_string {

  function query($group_by = FALSE) {
    $valuestring = urldecode($this->argument);
    $thisrealfield = _odata_toreal($this->real_field);
    $datetime = NULL;
    try{
      $datetime = new DateTime($valuestring, new DateTimeZone('UTC'));
      $this->query->add_ready_contextual_filter(_odata_date_equal($thisrealfield, $datetime));
    }
    catch (Exception $e) {
      $this->query->add_ready_contextual_filter('false');
    }
  }
}
