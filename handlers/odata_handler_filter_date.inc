<?php

/**
 * @file
 * Defines the various handler objects to help build and display oData views.
 */

/**
 * Extends views_handler_filter_date
 */
class OdataHandlerFilterDate extends views_handler_filter_date {

  /**
   * Add a type selector to the value form.
   */
  function value_form(&$form, &$form_state) {

    parent::value_form($form, $form_state);

    if (empty($form_state['exposed'])) {
      $form['value']['type']['#options'] = array(
        'date' => t('A date in a machine readable format. CCYY-MM-DDTHH:MM:SS is required.'),
      );
      $form['value']['value']['#description'] = 'e.g.,' . format_date(time(), 'odata_datetime_format');
    }
  }

  /**
   * Validate if our user has entered a valid date.
   */
  function options_validate(&$form, &$form_state) {
    parent::options_validate($form, $form_state);

    if (!empty($this->options['exposed']) && empty($form_state['values']['options']['expose']['required'])) {
      // Who cares what the value is if it's exposed and non-required.
      return;
    }

    $this->validate_valid_time($form['value'], $form_state['values']['options']['operator'], $form_state['values']['options']['value']);
  }

  /**
   * Validate that the time values convert to something usable.
   */
  function validate_valid_time(&$form, $operator, $value) {
    $operators = $this->operators();

    if ($operators[$operator]['values'] == 1) {
      $convert = format_date(strtotime($value['value']), 'odata_datetime_format');
      if (!empty($form['value']) && $value['value'] != $convert) {
        form_error($form['value'], t('Invalid date format.'));
      }
    }
    elseif ($operators[$operator]['values'] == 2) {
      $min = format_date(strtotime($value['min']), 'odata_datetime_format');
      if ($min != $value['min']) {
        form_error($form['min'], t('Invalid date format.'));
      }
      $max = format_date(strtotime($value['max']), 'odata_datetime_format');
      if ($max != $value['max']) {
        form_error($form['max'], t('Invalid date format.'));
      }
    }
  }

  function query() {

    switch ($this->options['operator']) {
      case '<':
        $expression = $this->real_field . '+lt+' . "datetime'" . $this->options['value']['value'] . "'";
        break;

      case '<=':
        $expression = $this->real_field . '+le+' . "datetime'" . $this->options['value']['value'] . "'";
        break;

      case '=':
        $expression = $this->real_field . '+eq+' . "datetime'" . $this->options['value']['value'] . "'";
        break;

      case '!=':
        $expression = $this->real_field . '+ne+' . "datetime'" . $this->options['value']['value'] . "'";
        break;

      case '>':
        $expression = $this->real_field . '+gt+' . "datetime'" . $this->options['value']['value'] . "'";
        break;

      case 'between':
        $expression = $this->real_field . '+gt+' . "datetime'" . $this->options['value']['min'] . "'+and+";
        $expression .= $this->real_field . '+lt+' . "datetime'" . $this->options['value']['max'] . "'";
        break;

      case 'not between':
        $expression = $this->real_field . '+lt+' . "datetime'" . $this->options['value']['min'] . "'+or+";
        $expression .= $this->real_field . '+gt+' . "datetime'" . $this->options['value']['max'] . "'";
        break;

      case '>=':
      default:
        $expression = $this->real_field . '+ge+' . "datetime'" . $this->options['value']['value'] . "'";
        break;
    }
    $this->query->add_where_expression($this->options['group'], $expression, NULL);
  }
}
