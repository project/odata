<?php
/**
 * @file
 * Definition of odata_handler_field_boolean.
 */

class OdataHandlerFieldBoolean extends views_handler_field_boolean {

  function query($group_by = FALSE) {
    $this->field_alias = $this->real_field;
    $this->query->select_fields[] = $this->real_field;
  }
}
