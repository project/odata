<?php
/**
 * @file
 * Adds a field as date
 */

/**
 * @todo refactor
 */
class OdataHandlerFieldComplexDate extends views_handler_field_date {

  function query() {
    $name = explode('-', $this->real_field);

    $this->query->select_fields[] = $name[0];
  }

  function render($values) {
    $name = explode('-', $this->real_field);
    $value = $values->{$name[0]}[$name[1]];
    $value = intval(intval(drupal_substr($value, 6)) / 1000);
    $values->{$this->real_field} = $value;
    $values->{$this->field_alias} = $value;
    $ret = parent::render($values);
    return $ret;
  }
}
