<?php
/**
 * @file
 * Definition of odata_handler_filter.
 */

class OdataHandlerField extends views_handler_field {

  function query($group_by = FALSE) {
    $this->field_alias = $this->real_field;
    $this->query->select_fields[] = $this->real_field;
  }
}
