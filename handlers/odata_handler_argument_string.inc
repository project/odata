<?php
/**
 * @file
 * This file contains the information of string arguments
 */

class OdataHandlerArgumentString extends views_handler_argument_string {

  function query($group_by = FALSE) {
    $valuestring = $this->argument;
    $thisrealfield = _odata_toreal($this->real_field);
    $this->query->add_ready_contextual_filter('\'' . urlencode($valuestring) . '\'' . '+eq+' . $thisrealfield);
  }
}
