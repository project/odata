<?php

/**
 * @file
 * Definition of odata_handler_filter_boolean_operator.
 */

class OdataHandlerFilterBooleanOperator extends views_handler_filter_boolean_operator {

  function query() {
    if ($this->value == 1) {
      $this->query->add_where($this->options['group'], $this->real_field, 'true', '+eq+');
    }
    else {
      $this->query->add_where($this->options['group'], $this->real_field, 'false', '+eq+');
    }
  }
}
