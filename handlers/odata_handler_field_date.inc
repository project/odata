<?php
/**
 * @file
 * Definition of odata_handler_field_date.
 */

/**
 * @todo refactor
 */
class OdataHandlerFieldDate extends views_handler_field_date {

  // Overrides query().
  function query() {
    $this->field_alias = $this->real_field;
    $this->query->select_fields[] = $this->real_field;
  }

  function render($values) {
    $values->{$this->real_field} = intval(intval(drupal_substr($values->{$this->real_field}, 6)) / 1000);
    $ret = parent::render($values);
    return $ret;
  }
}
